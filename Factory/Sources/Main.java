import Factory.UnitFactory;
import Factory.AlienFactory.AlienUnitFactory;
import Factory.HumanFactory.HumanUnitFactory;
import Units.UnitType;

public class Main {
    public static void main(String[] args) {

        UnitFactory unitFactory = new HumanUnitFactory();
        unitFactory.train(UnitType.SOLDIER);
        unitFactory.train(UnitType.MEDIC);

        unitFactory = new AlienUnitFactory();
        unitFactory.train(UnitType.SOLDIER);
        unitFactory.train(UnitType.MEDIC);

        unitFactory.train(UnitType.UNKNOWN);
    }
}
