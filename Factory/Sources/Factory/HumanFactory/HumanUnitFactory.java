package Factory.HumanFactory;
import Factory.UnitFactory;
import Units.Unit;
import Units.UnitType;
import Units.UnitTypeDoesNotExistException;
import Units.Medic.HumanMedic;
import Units.Soldier.HumanSoldier;

public class HumanUnitFactory extends UnitFactory {

    @Override
    protected Unit createUnit(UnitType unitType) throws UnitTypeDoesNotExistException {
        switch(unitType) {

            case SOLDIER:
            return new HumanSoldier();

            case MEDIC:
            return new HumanMedic();

            default:
            throw new UnitTypeDoesNotExistException();
        }        
    }
    
}
