package Factory;
import Units.Unit;
import Units.UnitType;
import Units.UnitTypeDoesNotExistException;

public abstract class UnitFactory {
    public Unit train(UnitType unitType) {
        Unit unit = null;
        try {
            unit = createUnit(unitType);
        } catch (UnitTypeDoesNotExistException e) {
            System.out.println(e);
            return null;
        }
        System.out.println("Training " + unit);
        return unit;
    }

    protected abstract Unit createUnit(UnitType unitType) throws UnitTypeDoesNotExistException;
}
