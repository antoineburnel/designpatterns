package Factory.AlienFactory;
import Factory.UnitFactory;
import Units.Unit;
import Units.UnitType;
import Units.UnitTypeDoesNotExistException;
import Units.Medic.AlienMedic;
import Units.Soldier.AlienSoldier;

public class AlienUnitFactory extends UnitFactory {

    @Override
    protected Unit createUnit(UnitType unitType) throws UnitTypeDoesNotExistException {
        switch(unitType) {

            case SOLDIER:
            return new AlienSoldier();

            case MEDIC:
            return new AlienMedic();

            default:
            throw new UnitTypeDoesNotExistException();
        }        
    }
    
}
