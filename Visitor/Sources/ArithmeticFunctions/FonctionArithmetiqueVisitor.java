package ArithmeticFunctions;
import ArithmeticExpressions.Constant.Constante;
import ArithmeticExpressions.Operators.Binary.Mult;
import ArithmeticExpressions.Operators.Binary.Plus;
import ArithmeticExpressions.Variable.Variable;

public interface FonctionArithmetiqueVisitor {
    public Object execute(Constante nombre);
    public Object execute(Variable variable);
    public Object execute(Plus plus);
    public Object execute(Mult mult);
}