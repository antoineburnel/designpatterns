package ArithmeticFunctions.Evaluation;
import ArithmeticExpressions.Constant.Constante;
import ArithmeticExpressions.Operators.Binary.Mult;
import ArithmeticExpressions.Operators.Binary.Plus;
import ArithmeticExpressions.Variable.TableOfVariables;
import ArithmeticExpressions.Variable.Variable;
import ArithmeticExpressions.Variable.VariableDoesNotExistsException;
import ArithmeticFunctions.FonctionArithmetiqueVisitor;

public class Evaluation implements FonctionArithmetiqueVisitor {

    private TableOfVariables _tableOfVariables;

    public Evaluation(TableOfVariables tableOfVariables) {
        _tableOfVariables = tableOfVariables;
    }

    @Override
    public Object execute(Constante nombre) {
        return nombre.getValue();
    }

    @Override
    public Object execute(Variable variable) {
        try {
            return variable.getValue(_tableOfVariables);
        } catch(VariableDoesNotExistsException e) {
            System.out.println(e);
            return 0f;
        }
    }

    @Override
    public Object execute(Plus plus) {
        return (double) plus.getOperand1().accept(this) + (double) plus.getOperand2().accept(this);
    }

    @Override
    public Object execute(Mult mult) {
        return (double) mult.getOperand1().accept(this) * (double) mult.getOperand2().accept(this);
    }
    
}
