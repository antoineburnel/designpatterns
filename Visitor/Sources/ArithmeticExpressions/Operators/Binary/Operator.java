package ArithmeticExpressions.Operators.Binary;

import ArithmeticExpressions.ExpressionArithmetique;

public abstract class Operator extends ExpressionArithmetique {

    private ExpressionArithmetique _ea1;
    private ExpressionArithmetique _ea2;

    public Operator(ExpressionArithmetique fa1, ExpressionArithmetique fa2) {
        _ea1 = fa1;
        _ea2 = fa2;
    }

    public ExpressionArithmetique getOperand1() {
        return _ea1;
    }

    public ExpressionArithmetique getOperand2() {
        return _ea2;
    }
}
