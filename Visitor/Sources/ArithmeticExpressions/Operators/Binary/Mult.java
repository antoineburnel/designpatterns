package ArithmeticExpressions.Operators.Binary;

import ArithmeticExpressions.ExpressionArithmetique;
import ArithmeticFunctions.FonctionArithmetiqueVisitor;

public class Mult extends Operator {
    public Mult(ExpressionArithmetique ea1, ExpressionArithmetique ea2) {
        super(ea1, ea2);
    }

    @Override
    public Object accept(FonctionArithmetiqueVisitor fa) {
        return fa.execute(this);
    }

    @Override
    public String toString() {
        return "(" + getOperand1() + " * " + getOperand2() + ")";
    }
}
