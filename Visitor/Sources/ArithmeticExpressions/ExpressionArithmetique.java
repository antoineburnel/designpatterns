package ArithmeticExpressions;

import ArithmeticFunctions.FonctionArithmetiqueVisitor;

public abstract class ExpressionArithmetique {
    public abstract Object accept(FonctionArithmetiqueVisitor fa);
}
