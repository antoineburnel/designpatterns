package ArithmeticExpressions.Constant;

import ArithmeticExpressions.ExpressionArithmetique;
import ArithmeticFunctions.FonctionArithmetiqueVisitor;

public class Constante extends ExpressionArithmetique {
    
    private double _value;

    public Constante(double value) {
        _value = value;
    }

    public double getValue() {
        return _value;
    }

    @Override
    public Object accept(FonctionArithmetiqueVisitor fa) {
        return fa.execute(this);
    }

    @Override
    public String toString() {
        return Double.toString(_value);
    }
}
