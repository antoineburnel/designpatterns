package ArithmeticExpressions.Variable;
import java.util.HashMap;

public class TableOfVariables {

    private HashMap<Character,Double> _variables;

    public TableOfVariables() {
        _variables = new HashMap<>();
    }

    public void setValue(char key, double value) {
        _variables.put(key, value);
    }

    public double getValue(char key) throws VariableDoesNotExistsException {
        Double value = _variables.get(key);
        if (value == null) throw new VariableDoesNotExistsException(key);
        return value;
    }

    public void removeValue(char key) {
        _variables.remove(key);
    }

    public void clear() {
        _variables.clear();
    }
}
