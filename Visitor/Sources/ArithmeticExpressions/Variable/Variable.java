package ArithmeticExpressions.Variable;

import ArithmeticExpressions.ExpressionArithmetique;
import ArithmeticFunctions.FonctionArithmetiqueVisitor;

public class Variable extends ExpressionArithmetique {

    private char _variable;

    public Variable(char variable) {
        _variable = variable;
    }

    public double getValue(TableOfVariables tableOfVariables) throws VariableDoesNotExistsException {
        return tableOfVariables.getValue(_variable);
    }

    @Override
    public Object accept(FonctionArithmetiqueVisitor fa) {
        return fa.execute(this);
    }

    @Override
    public String toString() {
        return Character.toString(_variable);
    }
}
