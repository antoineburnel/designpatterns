package ArithmeticExpressions.Variable;
public class VariableDoesNotExistsException extends Exception {
    
    public VariableDoesNotExistsException(char key) {
        super("The variable named \"" + key + "\" does has no value !");
    }
}
