import ArithmeticExpressions.ExpressionArithmetique;
import ArithmeticExpressions.Constant.Constante;
import ArithmeticExpressions.Operators.Binary.Mult;
import ArithmeticExpressions.Operators.Binary.Plus;
import ArithmeticExpressions.Variable.TableOfVariables;
import ArithmeticExpressions.Variable.Variable;
import ArithmeticFunctions.Evaluation.Evaluation;

public class Main {
    public static void main(String[] args) {
        
        ExpressionArithmetique ea = new Mult(new Plus(new Constante(2), new Variable('x')), new Constante(-4));
        
        TableOfVariables tableOfVariables = new TableOfVariables();
        tableOfVariables.setValue('x', 3);
        
        Evaluation evaluation = new Evaluation(tableOfVariables);

        String currentExpression = ea.toString();
        double currentResult = (double) ea.accept(evaluation);

        System.out.println(currentExpression + " = " + currentResult);
    }
}
