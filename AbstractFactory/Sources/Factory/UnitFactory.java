package Factory;
import Units.Medic.Medic;
import Units.Soldier.Soldier;

public interface UnitFactory {
    public Soldier createSoldier();
    public Medic createMedic();
}
