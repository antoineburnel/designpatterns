package Factory;
import Units.Medic.HumanMedic;
import Units.Medic.Medic;
import Units.Soldier.HumanSoldier;
import Units.Soldier.Soldier;

public class HumanUnitFactory implements UnitFactory {

    @Override
    public Soldier createSoldier() {
        return new HumanSoldier();
    }

    @Override
    public Medic createMedic() {
        return new HumanMedic();
    }

}
