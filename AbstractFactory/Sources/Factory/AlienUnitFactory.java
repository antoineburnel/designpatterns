package Factory;
import Units.Medic.AlienMedic;
import Units.Medic.Medic;
import Units.Soldier.AlienSoldier;
import Units.Soldier.Soldier;

public class AlienUnitFactory implements UnitFactory {

    @Override
    public Soldier createSoldier() {
        return new AlienSoldier();
    }

    @Override
    public Medic createMedic() {
        return new AlienMedic();
    }

}
