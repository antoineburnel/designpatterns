import Factory.AlienUnitFactory;
import Factory.HumanUnitFactory;
import Factory.UnitFactory;

public class Main {
    public static void main(String[] args) {

        UnitFactory unitFactory = new HumanUnitFactory();
        unitFactory.createSoldier().printName();
        unitFactory.createMedic().printName();

        unitFactory = new AlienUnitFactory();
        unitFactory.createSoldier().printName();
        unitFactory.createMedic().printName();
    }
}
